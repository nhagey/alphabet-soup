// command line: node index.js input.txt
const fs = require('fs');
const readline = require('readline');

// inputs from a data file and sorts data line by line
async function processLineByLine() {
    const fileName = process.argv.slice(2); //get filename from command line
    if (fileName.length == 0){ //check if the filename is in the command line
        console.log("error: filename required")
        return;
    }

  const fileStream = fs.createReadStream(fileName[0]); //input file
  const rl = readline.createInterface({ //read line from file
    input: fileStream,
    crlfDelay: Infinity
  });

  var rows; //variables to save size
  var i = 0; //iterator for input rows 
  var k = 0; //interator for grid 
  let characterGrid = []; //stores the grid
  let searchWords = []; //array for words to look for

  for await (const line of rl) {  //loop through each line of the file
    var data = `${line}` //save the line to a string
    if (i == 0){ //if it's the first line, save the size
      const size = data.split("x");
      rows = size[0]
    }

    if (i > 0 && i <= rows){ //if it's the grid, save in the grid variable
      const singleCharacters = data.split(" "); //split the gird line and save in an array
      for (var j = 0; j < singleCharacters.length; j++) { 
        characterGrid[i-1] = singleCharacters; 
      }
    }

    if (i>rows){ //create an array of words to search
      searchWords[k] = data;
      k++;
    }

    i++;
  }
  findWords(characterGrid, searchWords);
}

// find Words looks for words in the character grid
function findWords(grid, words){
  for (let word of words) { //get each letter of the search word

    for (var i = 0; i < grid.length; i++) {    // look for letters in the grid
      var gridLine = grid[i];
      for (var j = 0; j < gridLine.length; j++) {
        if (grid[i][j] == word[0]){ //find first letter
          checkDiagonal(word, grid, i, j);
          checkHorizontal(word, grid, i, j);
          checkVertical(word, grid, i, j);

          checkDiagonalBackwards(word, grid, i, j)
          checkHorizontalBackwards(word, grid, i, j)
          checkVerticalBackwards(word, grid, i, j)
        }
      }
    }
  }
}

function checkDiagonal(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');
  
  for (var k=1;k < word.length && i+k < grid.length;k++){
    if (grid[i+k][j+k] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j+k;
        var col = i+k;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

function checkHorizontal(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');

  for (var k=1;k < word.length && i < grid.length;k++){
    if (grid[i][j+k] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j+k;
        var col = i;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

function checkVertical(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');

  for (var k=1;k < word.length && i+k < grid.length;k++){
    if (grid[i+k][j] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j;
        var col = i+k;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

function checkDiagonalBackwards(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');

  for (var k=1;k < word.length && i-k > 0;k++){
    if (grid[i-k][j-k] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j-k;
        var col = i-k;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

function checkHorizontalBackwards(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');

  for (var k=1;k < word.length && i > 0;k++){
    if (grid[i][j-k] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j-k;
        var col = i;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

function checkVerticalBackwards(word, grid, i, j){
  var originalWord = word;
  word = word.replace(/\s/g, '');

  for (var k=1;k < word.length && i-k > 0;k++){
    if (grid[i-k][j] == word[k]){
      if (word[k] == word.charAt(word.length - 1)){ //get position of last character
        var row = j;
        var col = i-k;
        console.log(originalWord + " " + j + ":" + i + " " + row + ":" + col)
      }
    }else{
      break;
    }
  }
}

processLineByLine();